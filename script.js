

class Employee{
    constructor(name,age,salary){
        this.name = name
        this.age=age
        this.salary = salary
    }
    set name(value){
        if(value.length>2){
            this._name = value;
        }else{
            this._name = "Noname"
        }
    }get name(){
        return this._name
    }

    set age(value){
        if(value>=18){
            this._age = value
        }else{
            this._age = 'You are so small'
        }
    }
    get age(){
        return this._age
    }

    set salary(value){
        this._salary = value * 20
    }
    get salary(){
        return this._salary + 'грн'
    }
}


const pers = new Employee('a',18,1000)
console.log(pers.salary);

class Programmer extends Employee{
    constructor(name,age,salary,lang){
        super(name,age,salary)
        this.lang = lang;
    }
    set salary (value){
        this._salary = value * 20 * 3;
    }
    get salary (){
        return this._salary + "$"
    }
}

const prog = new Programmer('andriy',19,1000,'javascript')

console.log(prog.salary);